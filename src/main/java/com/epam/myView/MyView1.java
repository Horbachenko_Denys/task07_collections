package com.epam.myView;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView1 {


    public static Logger logger = LogManager.getLogger(MyView1.class);
    private static Scanner input = new Scanner(System.in);
    private Map<MyEnum1, String> menu;

    public static void main(String[] args) {
        new MyView1().show();
    }
    public MyView1() {
        menu = new LinkedHashMap<>();
        menu.put(MyEnum1.ONE, " 1 - print house list");
        menu.put(MyEnum1.TWO, " 2 - sort house list by price");
        menu.put(MyEnum1.THREE, " 3 - sort house list by distance to kindergarten");
        menu.put(MyEnum1.FOUR, " 4 - sort house list by distance to school");
        menu.put(MyEnum1.FIVE, " 5 - sort house list by distance to playground");
        menu.put(MyEnum1.QUIT, " Q - Exit");

    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                System.out.println("You press button: " + keyMenu);
            } catch (Exception e) {
                logger.error("Error in view");
            }
        } while (!keyMenu.equals("Q"));
    }
}
