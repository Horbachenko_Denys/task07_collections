package com.epam.myBinaryTree;

import com.epam.myBinaryTree.model.MyBinaryTree;
import com.epam.myBinaryTree.view.View;

public class Application {
    public static void main(String[] args) {
        View view = new View();
        view.runView();
    }
}
