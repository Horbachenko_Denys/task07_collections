package com.epam.myBinaryTree.view;

import com.epam.myBinaryTree.controller.Controller;

import java.util.Scanner;

public class View {
    private Controller controller;
    private Scanner scanner;

    public View() {
        controller = new Controller();
    }

    public void runView() {
        addCat();

        while (true) {
                System.out.println("Enter command:\n" +
                        "0) Add cat.\n" +
                        "1) Get cat.\n" +
                        "2) Remove cat.\n" +
                        "Type 'exit' if done");
                String cmd = scanner.nextLine();

                if (cmd.equals("exit")) break;
                if (cmd.equals("0")) addCat();
                if (cmd.equals("1")) getCat();
                if (cmd.equals("2")) removeCat();
        }
    }

    private void addCat(){
        scanner = new Scanner(System.in);

        System.out.println("Enter cat key number:");
        int index = Integer.parseInt(scanner.nextLine());
        System.out.println("Enter cat name:");
        String catName = scanner.nextLine();
        System.out.println("Enter cat age:");
        int catAge = Integer.parseInt(scanner.nextLine());
        controller.saveCat(catName, catAge, index);
    }

    private void getCat(){
        scanner = new Scanner(System.in);

        System.out.println("Enter cat key number :");
        int index = Integer.parseInt(scanner.nextLine());
        System.out.println(controller.getCat(index));
    }

    private void removeCat(){
        scanner = new Scanner(System.in);

        System.out.println("Enter cat key number :");
        int index = Integer.parseInt(scanner.nextLine());
        controller.removeCat(index);
    }

}
