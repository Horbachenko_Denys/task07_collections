package com.epam.myBinaryTree.controller;

import com.epam.myBinaryTree.model.Cat;
import com.epam.myBinaryTree.model.MyBinaryTree;

public class Controller {
    private MyBinaryTree <Integer, Cat> cats;

    public Controller() {
        cats = new MyBinaryTree<>();
    }

    public void saveCat(String name, int age, int index){
        Cat cat = new Cat(name, age);
        cats.put(index, cat);
    }

    public Cat getCat(int index){
        return cats.get(index);
    }

    public void removeCat(int index){
    }
}
